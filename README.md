# README #

### What is this repository for? ###

Repository containing the report and code for my 3rd Year Computer Science Project on Personalised Learning-to-rank for e-commerce search.

Please consider reading the report in the root directory for a detailed description of the project.