#!/usr/bin/python

import os,sys

def parse(filename):
    inddict={}
    countdict = {}
    for l in open(filename, 'r'):
        features=l.rstrip().split(' ')
        dataline = [str(0)] * 702;
        
        dataline[0] = str(features[0])
        dataline[1] = str(features[1].split(':')[1])

        for (i,f) in enumerate(features):
            if i > 1:
                indfeat = f.split(':')
                if indfeat[0] != 'qid':
                    dataline[int(indfeat[0]) + 1] = str(indfeat[1]);
    
        print ",".join(map(str,dataline))

if len(sys.argv)<2:
    print "Usage: python txt2csv.py train/set1.train.txt"
else:
    parse(sys.argv[1])
