#!/bin/bash

# set the number of nodes and processes per node
#PBS -l nodes=1:ppn=16

# set max wallclock time
#PBS -l walltime=100:00:00

# set name of job
#PBS -N rankNetDataParsing

# mail alert at start, end and abortion of execution
#PBS -m bea

# send mail to this address
#PBS -M daniel.vasile@oriel.ox.ac.uk

# use submission environment
#PBS -V

cd /data/coml-pssr/orie3215/workspace/learning-to-rank/ltrc_yahoo

# load torch
module load torch

# load python
module load python



# # parse train txt into pairs txt
# python examples2pairs.py '~$set1.train.txt' 3500000 60 > '~$set1.train.pairs.txt'

# # parse train pairs txt into csv
# python textpairs2csv.py '~$set1.train.pairs.txt' > '~$set1.train.pairs.csv'

# # parse train csv to t7
# ./read_train_csv.lua



# parse test txt into pairs txt
python examples2pairs.py '~$set1.test.txt' 500000 30 > '~$set1.testSmall.pairs.txt'

# parse test pairs txt into csv
python textpairs2csv.py '~$set1.testSmall.pairs.txt' > '~$set1.testSmall.pairs.csv'

# parse test csv to t7
./read_test_csv.lua



# # parse valid txt into pairs txt
# python examples2pairs.py '~$set1.valid.txt' 5000000 60 > '~$set1.valid.pairs.txt'

# # parse valid pairs txt into csv
# python textpairs2csv.py '~$set1.valid.pairs.txt' > '~$set1.valid.pairs.csv'

# # parse valid csv to t7
# ./read_valid_csv.lua
