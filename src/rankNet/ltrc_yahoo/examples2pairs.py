#!/usr/bin/python

import os,sys,math

def parse(filename, maxLoad, maxQueryLoad):
    lastQid = ''
    docs = []
    pairs = 0

    for l in open(filename, 'r'):
        features = l.rstrip().split(' ')
        qid = features[1].split(':')[1]

        if qid == lastQid or lastQid == '':
            if len(docs) < maxQueryLoad or maxQueryLoad == -1:
                docs.append(features)
            lastQid = qid
        else:
            for a in range(0, len(docs)):
                A = docs[a]
                oA = int(A[0])

                for b in range (a+1, len(docs)):
                    B = docs[b]
                    oB = int(B[0])
                    
                    oAB = float (oA - oB)
                    e_oAB = math.exp(oAB)
                    pAB = e_oAB / (1 + e_oAB)

                    # Data entry format: P(A > B), qID, featureA1, ... , feature A700, feature B1, ... , featureB700
                    pairFeatures = [str(pAB)] + A[1:len(A)] + B[2:len(B)]

                    print " ".join(map(str,pairFeatures))
                    pairs += 1
            
            lastQid = qid
            docs = [features]

        if maxLoad > 0 and pairs > maxLoad:
            break;

if len(sys.argv)<2:
    print "Usage: python examples2pairs.py set1.train.txt maxLoad maxQueryLoad"
else:
    parse(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]))
