#!/usr/bin/python

import os,sys

def parse(filename):
    inddict={}
    countdict = {}
    for l in open(filename, 'r'):
        features=l.rstrip().split(' ')
        dataline = [0] * 700;
        for (i,f) in enumerate(features):
            if i==0:
                dataline[0] = int(f);
            else:
                indfeat = f.split(':')
                if indfeat[0] != 'qid':
                    dataline[int(indfeat[0])] = indfeat[1];
    
        print ",".join(map(str,dataline))

if len(sys.argv)<2:
    print "Usage: python txt2svmlight.py train/set1.train.txt"
else:
    parse(sys.argv[1])
