#!/usr/bin/python

import os,sys

def parse(filename):
    for l in open(filename, 'r'):
        features = l.rstrip().split(' ')
        dataline = [str(0)] * 1402;
        
        dataline[0] = str(features[0])
        dataline[1] = str(features[1].split(':')[1])

        prevFId = 0
        idOff = 0
        for (i,f) in enumerate(features):
            if i > 1:
                indfeat = f.split(':')
                if indfeat[0] != 'qid':
                    fId = int(indfeat[0]) + 1 + idOff
                    if idOff == 0 and fId < prevFId:
                        idOff = 700
                    prevFId = fId

                    dataline[fId] = str(indfeat[1]);
    
        print ",".join(map(str,dataline))

if len(sys.argv)<2:
    print "Usage: python textpairs2csv.py train/set1.pairs.train.txt"
else:
    parse(sys.argv[1])
