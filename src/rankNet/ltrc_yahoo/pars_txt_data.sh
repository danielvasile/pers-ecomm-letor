#!/bin/bash

# set the number of nodes and processes per node
#PBS -l nodes=1:ppn=16

# set max wallclock time
#PBS -l walltime=100:00:00

# set name of job
#PBS -N rankNetDataParsing

# mail alert at start, end and abortion of execution
#PBS -m bea

# send mail to this address
#PBS -M daniel.vasile@oriel.ox.ac.uk

# use submission environment
#PBS -V

cd /data/coml-pssr/orie3215/workspace/learning-to-rank/ltrc_yahoo

# load torch
module load torch

# load python
module load python


# parse valid txt into csv
python txt2csv.py '~$set1.valid.txt' > '~$set1.valid.csv'

# parse valid csv to t7
./read_valid_csv.lua
