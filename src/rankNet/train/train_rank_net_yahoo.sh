#!/bin/bash

# set the number of nodes and processes per node
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16

# request memory
#SBATCH --mem 128000

# set max wallclock time
#SBATCH --time=20:00:00

# set name of job
#SBATCH --job-name=rankNetTrainingYah

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=ALL

# send mail to this address
#SBATCH --mail-user=daniel.vasile@oriel.ox.ac.uk

# use submission environment

cd /data/coml-pssr/orie3215/workspace/rankNet/train

# load torch
module load torch

# start training
th /data/coml-pssr/orie3215/workspace/rankNet/train/rank_net.lua -train_size 1500000 -valid_size 500000 > trainResultsYahoo.txt
