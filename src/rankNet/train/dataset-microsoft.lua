require 'torch'
require 'paths'

microsoft = {}

microsoft.path_dataset = '../microsoft_data'
microsoft.path_trainset = paths.concat(microsoft.path_dataset, '~$train.t7')
microsoft.path_testset = paths.concat(microsoft.path_dataset, '~$test.t7')
microsoft.path_validset = paths.concat(microsoft.path_dataset, '~$valid.t7')

function microsoft.loadTrainSet(maxLoad, shuffle)
   return microsoft.loadDataset(microsoft.path_trainset, maxLoad, shuffle)
end

function microsoft.loadTestSet(maxLoad, shuffle)
   return microsoft.loadDataset(microsoft.path_testset, maxLoad, shuffle)
end

function microsoft.loadValidSet(maxLoad, shuffle)
   return microsoft.loadDataset(microsoft.path_validset, maxLoad, shuffle)
end

function microsoft.loadDataset(fileName, maxLoad, shuffle)
   local f = torch.load(fileName)
   local data = f:type(torch.getdefaulttensortype())

   if shuffle == 'true' then
        -- shuffle the dataset
        local shuffled_indices = torch.randperm(data:size(1)):long()
        -- create a shuffled *copy*, with a new storage
        data = data:index(1, shuffled_indices):squeeze()
   end

   local nExample = f:size(1)
   if maxLoad and maxLoad > 0 and maxLoad < nExample then
      nExample = maxLoad
      print('<microsoft> loading only ' .. nExample .. ' examples')
   end

   -- NOTE: Lua indexes data from 1 (crazy!?)
   -- Data entry format: P(A > B), qID, featureA1, ... , feature A136, feature B1, ... , featureB136
   data = data[{{1,nExample}}]

   -- Extract P(A > B) in 'targets' tensor and remove qID from the feature vector
   local targets = data:select(2, 1)
   data = data[{{}, {3, 274}}]

   local dataset = {}

   collectgarbage()
   dataset.data = torch.Tensor(2, data:size(1), 136)
   for i=1, data:size(1) do
      dataset.data[1][i] = data[{ i, {1, 136}}]
      dataset.data[2][i] = data[{ i, {137, 272}}]

      if i % 200 == 0 then
         collectgarbage()
      end
   end
   dataset.targets = targets

   collectgarbage()
   print('<microsoft> done')

   function dataset:normalize(mean, std_)
      local mean = mean or data:view(data:size(1), -1):mean(1)
      local std = std_ or data:view(data:size(1), -1):std(1, true)
      for i=1,data:size(1) do
         data[i]:add(-mean[1][i])
         if std[1][i] > 0 then
            tensor:select(2, i):mul(1/std[1][i])
         end
      end
      return mean, std
   end

   function dataset:normalizeGlobal(mean_, std_)
      local std = std_ or data:std()
      local mean = mean_ or data:mean()
      data:add(-mean)
      data:mul(1/std)
      return mean, std
   end

   function dataset:size()
      return nExample
   end

   setmetatable(dataset, {__index = function(self, index)
			     local input = self.data[{{}, index}]
			     local target = self.targets[index]
			     local example = {input, target}
                                       return example
   end})

   return dataset
end
