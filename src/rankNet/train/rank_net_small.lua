require 'torch'
require 'math'
require 'nn'
require 'optim'
require 'gnuplot'
require 'dataset-yahoo-small'

---------------------------------------------------------------------------------
----- RankNet implementation - Based on Learning to Rank using Grad Descent -----

---- Parts of the code have been extracted from a ML Practical Exercise
---- https://www.cs.ox.ac.uk/teaching/materials14-15/ml/practicals/practical3.pdf

---------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- INITIALIZATION AND DATA
------------------------------------------------------------------------------

torch.manualSeed(1)     -- fix random seed so program runs the same every time

-- TODO: play with these optimizer options
local opt = {}          -- these options are used throughout
opt.optimization = 'lbfgs'
opt.train_size = 0      -- set to 0 to use all training data
opt.test_size = 0       -- 0 means load all data
opt.valid_size = 0      -- 0 means load all data
opt.batch_size = 0      -- 0 means load all data
opt.epochs = 50         -- approximate number of passes through the training data

-- NOTE: the code below changes the optimization algorithm used, and its settings
local optimState       -- stores a lua table with the optimization algorithm's settings, and state during iterations
local optimMethod      -- stores a function corresponding to the optimization routine

if opt.optimization == 'lbfgs' then
  optimState = {
    learningRate = 0.1,
    maxIter = 2,
    nCorrection = 100
  }
  optimMethod = optim.lbfgs
elseif opt.optimization == 'sgd' then
  optimState = {
    learningRate = 1e-1,
    weightDecay = 0,
    momentum = 0,
    learningRateDecay = 1e-3
  }
  optimMethod = optim.sgd
elseif opt.optimization == 'adagrad' then
  optimState = {
    learningRate = 1e-2,
  }
  optimMethod = optim.adagrad
else
  error('Unknown optimizer')
end

-- load dataset using dataset-yahoo.lua into tensors (first dim of data/targets ranges over data)
-- data points are pairs of feature vectors of size ~700 describing pairs of documents (A, B) resulted under
-- the same query. 
local function load_dataset(train_or_test, count)
    local data
    if train_or_test == 'train' then
        data = yahoo.loadTrainSet(count)
    elseif train_or_test == 'valid' then
        data = yahoo.loadValidSet(count)
    else
        data = yahoo.loadTestSet(count)
    end

    -- shuffle the dataset
    local shuffled_indices = torch.randperm(data.data:size(2)):long()
    
    -- create a shuffled *copy*, with a new storage
    data.data[1] = data.data[1]:index(1, shuffled_indices):squeeze()
    data.data[2] = data.data[2]:index(1, shuffled_indices):squeeze()
    data.targets = data.targets:index(1, shuffled_indices):squeeze()

    -- TODO: Normalise data
    -- data:normalize()

    collectgarbage()

    print('--------------------------------')
    print(' loaded dataset "' .. train_or_test .. '"')
    print('inputs', data.data:size(2))
    print('targets', data.targets:size())
    print('--------------------------------')

    return data
end

local train = load_dataset('train', opt.train_size)
-- local test = load_dataset('test', opt.test_size)
local valid = load_dataset('valid', opt.valid_size)

------------------------------------------------------------------------------
-- MODEL
------------------------------------------------------------------------------

local n_train_data = train.data:size(2)       -- number of training data
local n_inputs = train.data[1][1]:size(1)     -- number of inputs to a linearRanker = number of dims of input (~700 in case of yahoo feature vectors)

if opt.batch_size == 0 then
  opt.batch_size = n_train_data
end

-- TODO: Use sparse layers!
local lin_layer1 = nn.Linear(n_inputs, 20)
local lin_layer2 = nn.Linear(20, 1)

local linearRanker = nn.Sequential()

-- TODO: Experiment with a single / less wide linear layer to make training faster
-- TODO: Experiment with a deep architecture
-- TODO: Experiment without shared params

linearRanker:add(lin_layer1)
linearRanker:add(nn.Sigmoid())
linearRanker:add(lin_layer2)
linearRanker:add(nn.Sigmoid())
linearRanker:add(nn.MulConstant(4))

local linearRanker_clone = linearRanker:clone('weight','bias','gradWeight','gradBias')

local pairRanker = nn.ParallelTable()
pairRanker:add(linearRanker)
pairRanker:add(linearRanker_clone)

local model = nn.Sequential()

model:add(pairRanker)
model:add(nn.CSubTable())
model:add(nn.Sigmoid()) -- Outputs f(A, B) = P(A > B) that A is ranked higher than B

------------------------------------------------------------------------------
-- LOSS FUNCTION
------------------------------------------------------------------------------

local criterion = nn.BCECriterion() -- Binary Cross Entropy

------------------------------------------------------------------------------
-- TRAINING
------------------------------------------------------------------------------

local parameters, gradParameters = model:getParameters()

------------------------------------------------------------------------
-- Define closure with mini-batches 
------------------------------------------------------------------------

local counter = 0
local feval = function(x)
  if x ~= parameters then
    parameters:copy(x)
  end

  -- get start/end indices for our minibatch
  --           ------- 
  --          |  ...  |
  --        ^ ---------<- start index = i * batchsize + 1
  --  batch | |       |
  --   size | | batch |       
  --        v |   i   |<- end index (inclusive) = start index + batchsize
  --          ---------                         = (i + 1) * batchsize + 1
  --          |  ...  |                 (except possibly for the last minibatch, we can't 
  --          --------                   let that one go past the end of the data, so we take a min())
  
  local start_index = counter * opt.batch_size + 1
  local end_index = math.min(n_train_data, (counter + 1) * opt.batch_size + 1)
  
  if end_index == n_train_data then
    counter = 0
  else
    counter = counter + 1
  end

  local batch_inputs = train.data[{{}, {start_index, end_index}, {}}]
  -- print (batch_inputs)
  local batch_targets = train.targets[{{start_index, end_index}}]
  -- print (batch_targets)
  gradParameters:zero()

  -- In order, these lines compute:
  -- 1. compute outputs (probabilities A > B) for each pair data point
  local batch_outputs = model:forward(batch_inputs)
  -- print (batch_outputs)
  -- 2. compute the loss of these outputs, measured against the true targets in batch_target
  local batch_loss = criterion:forward(batch_outputs, batch_targets)
  -- 3. compute the derivative of the loss wrt the outputs of the model
  local dloss_doutput = criterion:backward(batch_outputs, batch_targets) 
  -- 4. use gradients to update weights, we'll understand this step more next week
  model:backward(batch_inputs, dloss_doutput)

  -- optim expects us to return
  --     loss, (gradient of loss with respect to the weights that we're optimizing)
  return batch_loss, gradParameters
end
  
------------------------------------------------------------------------
-- OPTIMIZE
------------------------------------------------------------------------
local losses = {}          -- training losses for each iteration/minibatch
local validLosses = {}
local epochs = opt.epochs  -- number of full passes over all the training data
local iterations = epochs * math.ceil(n_train_data / opt.batch_size) -- integer number of minibatches to process
local prevEpoch = -1

-- In each iteration, we:
--    1. call the optimization routine, which
--      a. calls feval(parameters), which
--          i. grabs the next minibatch
--         ii. returns the loss value and the gradient of the loss wrt the parameters, evaluated on the minibatch
--      b. the optimization routine uses this gradient to adjust the parameters so as to reduce the loss.
--    3. then we append the loss to a table (list) and print it
for i = 1, iterations do
  -- optimMethod is a variable storing a function, either optim.sgd or optim.adagrad or ...
  -- it returns (new_parameters, table), where table[0] is the value of the function being optimized
  -- and we can ignore new_parameters because `parameters` is updated in-place every time we call 
  -- the optim module's function. It uses optimState to hide away its bookkeeping that it needs to do
  -- between iterations.
  local _, minibatch_loss = optimMethod(feval, parameters, optimState)

  -- Since we evaluate the loss on a different minibatch each time, the loss will sometimes 
  -- fluctuate upwards slightly (i.e. the loss estimate is noisy).
  if (i * opt.batch_size) / n_train_data > prevEpoch then -- print the loss at the end of each epoch
      prevEpoch = prevEpoch + 1
      losses[#losses + 1] = minibatch_loss[1] -- append the new loss

      local validPairProbs = model:forward(valid.data)
      local validLoss = criterion:forward(validPairProbs, valid.targets)
      validLosses[#validLosses + 1] = validLoss

      print(string.format("%.0f,%.6f,%.6f", prevEpoch, minibatch_loss[1], validLoss))
  end

  -- if i % 20 == 0 then
  --   collectgarbage()
  -- end
  collectgarbage()
end

-- Turn table of losses into a torch Tensor, and plot it
-- gnuplot.plot({
--  torch.range(1, #losses),        -- x-coordinates for data to plot, creates a tensor holding {1,2,3,...,#losses}
--  torch.Tensor(losses),           -- y-coordinates (the training losses)
--  '-'},
--  {
--  torch.range(1, #validLosses),        -- x-coordinates for data to plot, creates a tensor holding {1,2,3,...,#losses}
--  torch.Tensor(validLosses),           -- y-coordinates (the training losses)
--  '-'})

------------------------------------------------------------------------------
-- TESTING THE LEARNED MODEL
------------------------------------------------------------------------------

-- TODO: Compare wits expected test probs
-- TODO: Extract documents' ranks and output ranked data for a specific qID
-- TODO: Rebuild the ranking by using the the outputs of the linearRanker
-- TODO: compute test ranking error here using a normalized discounted cumulative gain measure (NDCG)

-- local testPairProbs = model:forward(test.data)
-- local testLoss = criterion:forward(testPairProbs, test.targets)
-- print(string.format("Test loss: %.6f", testLoss))

-- Save the trained model:
torch.save('trainedLinearRanker.t7', linearRanker)
