require 'torch'
require 'math'
require 'nn'
require 'optim'
require 'gnuplot'
require 'dataset-diginetica'

---------------------------------------------------------------------------------
----- EnsembleNet implementation (based on RankNet)

---- Parts of the code have been extracted from an Oxford ML Practical Exercise
---- https://www.cs.ox.ac.uk/teaching/materials14-15/ml/practicals/practical3.pdf

---------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- INITIALIZATION AND DATA
------------------------------------------------------------------------------

cmd = torch.CmdLine()
cmd:option('-epochs', '30', 'number of epochs')
cmd:option('-train_size', '15000000', 'number of training pairs')
cmd:option('-valid_size', '3000000', 'number of validation pairs')
cmd:option('-rankers', 'full', 'list of rankers to build ensemble on top')
cmd:option('-ensmode', 'simpleNet', 'the ensembling mechanism: hybrid, simpleNet, complexNet')
params = cmd:parse(arg)

torch.manualSeed(1)     -- fix random seed so program runs the same every time

local opt = {}
opt.optimization = 'adagrad'
opt.train_size = tonumber(params.train_size)      -- set to 0 to use all training data
opt.test_size = 0                                 -- 0 means load all data
opt.valid_size = tonumber(params.valid_size)      -- 0 means load all data
opt.batch_size = 10000                                -- 0 means load all data
opt.epochs = tonumber(params.epochs)     -- approximate number of passes through the training data

local optimState
local optimMethod

if opt.optimization == 'lbfgs' then
  optimState = {
    learningRate = 1e-2,
    maxIter = 2,
    nCorrection = 100
  }
  optimMethod = optim.lbfgs
elseif opt.optimization == 'sgd' then
  optimState = {
    learningRate = 10,
    weightDecay = 0,
    momentum = 0,
    learningRateDecay = 1e-3
  }
  optimMethod = optim.sgd
elseif opt.optimization == 'adagrad' then
  optimState = {
    learningRate = 1,
    learningRateDecay = 1e-2
  }
  optimMethod = optim.adagrad
else
  error('Unknown optimizer')
end

local function load_dataset(train_or_valid, count, rankers)
    local dataset = diginetica

    local data
    if train_or_valid == 'train' then
        data = dataset.loadTrainSet(count, opt.shuffle, 'full', params.ensmode, rankers)
    elseif train_or_valid == 'valid' then
        data = dataset.loadValidSet(count, opt.shuffle, 'full', params.ensmode, rankers)
    end

    collectgarbage()

    print('--------------------------------')
    print(' loaded dataset "' .. train_or_valid .. '"')
    print('inputs', data.data:size(2))
    print('targets', data.targets:size())
    print('--------------------------------')

    return data
end

--- Load the rankers to be part of the ensemble
local rankers = diginetica.loadRankers(params.rankers)

--- Process the train and valid data by running the trained rankers on it
local train = load_dataset('train', opt.train_size, rankers)
local valid = load_dataset('valid', opt.valid_size, rankers)

------------------------------------------------------------------------------
-- Ensemble MODEL
------------------------------------------------------------------------------

local ensembleRanker = nn.Sequential()
local n_train_data = train.data:size(2)       -- number of training samples

if opt.batch_size == 0 then
  opt.batch_size = n_train_data
end


if params.ensmode == 'simpleNet' then
    local n_inputs = #rankers     -- number of inputs to a the ensemble head (#rankers for simpleNet)

    local lin_layer1 = nn.Linear(n_inputs, 1)
    local lin_layer2 = nn.Linear(50, 1)

    ensembleRanker:add(lin_layer1)
--    ensembleRanker:add(nn.Sigmoid())
--    ensembleRanker:add(lin_layer2)
--    ensembleRanker:add(nn.Sigmoid())
end
-- TODO: Model the other types of ensembling heads.

local ensembleRanker_clone = ensembleRanker:clone('weight','bias','gradWeight','gradBias')

local pairRanker = nn.ParallelTable()
pairRanker:add(ensembleRanker)
pairRanker:add(ensembleRanker_clone)

local model = nn.Sequential()

model:add(pairRanker)
model:add(nn.CSubTable())
model:add(nn.Sigmoid()) -- Outputs f(A, B) = P(A > B) that A is ranked higher than B

------------------------------------------------------------------------------
-- LOSS FUNCTION
------------------------------------------------------------------------------

local criterion = nn.BCECriterion() -- Binary Cross Entropy

------------------------------------------------------------------------------
-- TRAINING
------------------------------------------------------------------------------

local parameters, gradParameters = model:getParameters()

------------------------------------------------------------------------
-- Backpropagation
------------------------------------------------------------------------

local counter = 0
local feval = function(x)
  if x ~= parameters then
    parameters:copy(x)
  end

  local start_index = counter * opt.batch_size + 1
  local end_index = math.min(n_train_data, (counter + 1) * opt.batch_size + 1)
  
  if end_index == n_train_data then
    counter = 0
  else
    counter = counter + 1
  end

  local batch_inputs = train.data[{{}, {start_index, end_index}, {}}]
  local batch_targets = train.targets[{{start_index, end_index}}]
  gradParameters:zero()

  local batch_outputs = model:forward(batch_inputs)
  local batch_loss = criterion:forward(batch_outputs, batch_targets)

  local dloss_doutput = criterion:backward(batch_outputs, batch_targets) 

  model:backward(batch_inputs, dloss_doutput)

  return batch_loss, gradParameters
end

------------------------------------------------------------------------
-- Loss evaluation
------------------------------------------------------------------------

local eval = function(data, targets, batch_size, model)
    local n_data = data:size(2)
    local iters =  math.ceil(n_data / batch_size)

    local pairProbs = model:forward(data[{{}, {1, math.min(n_data, batch_size)}, {}}])

    for i=2, iters do
        local start_index = (i-1) * batch_size + 1
        local end_index = math.min(n_data, i * batch_size)

        local batch_inputs = data[{{}, {start_index, end_index}, {}}]
        pairProbs = torch.cat(pairProbs, model:forward(batch_inputs), 1)
    end

    local loss = criterion:forward(pairProbs, targets)
    collectgarbage()

    return loss
end

------------------------------------------------------------------------
-- OPTIMIZE
------------------------------------------------------------------------
local epochs = opt.epochs
local iterations = epochs * math.ceil(n_train_data / opt.batch_size)
local prevEpoch = -1

for i = 1, iterations do
  local _, _ = optimMethod(feval, parameters, optimState)

  if (i * opt.batch_size) / n_train_data > prevEpoch then -- print the loss at the end of each epoch
      prevEpoch = prevEpoch + 1

      model:evaluate()

      local validLoss = eval(valid.data, valid.targets, opt.batch_size, model)
      local trainLoss = eval(train.data, train.targets, opt.batch_size, model)

      model:training()

      print(string.format("%.0f,%.6f,%.6f", prevEpoch, trainLoss, validLoss))
  end

  collectgarbage()
end

------------------------------------------------------------------------------
-- SAVING THE LEARNED MODEL
------------------------------------------------------------------------------

torch.save('~$trainedLinearRankerdiginetica-ens' .. '-' .. params.ensmode .. '.t7', ensembleRanker)
