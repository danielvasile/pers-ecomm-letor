require 'torch'
require 'paths'

----- Diginetica Dataset manager for RankNet -----

diginetica = {}

diginetica.path_dataset = '../../diginetica-challenge/dataset'
diginetica.path_trainset = paths.concat(diginetica.path_dataset, '~$train-qi-pairs')
diginetica.path_validset = paths.concat(diginetica.path_dataset, '~$valid-qi-pairs')

diginetica.path_testunits = paths.concat(diginetica.path_dataset, '~$test-qi-features')
diginetica.path_validunits = paths.concat(diginetica.path_dataset, '~$valid-qi-features')

--- Functions for load paired sets
function diginetica.loadTrainSet(maxLoad, shuffle, subset, ensmode, rankers)
   return diginetica.loadDataset(diginetica.path_trainset, maxLoad, subset, ensmode, rankers)
end

function diginetica.loadValidSet(maxLoad, shuffle, subset, ensmode, rankers)
   return diginetica.loadDataset(diginetica.path_validset, maxLoad, subset, ensmode, rankers)
end

--- Functions for loading unit sets
function diginetica.loadTestUnits(maxLoad, shuffle, subset, ensmode, rankers)
   return diginetica.loadUnits(diginetica.path_testunits, maxLoad, 'full', ensmode, rankers)
end

function diginetica.loadValidUnits(maxLoad, shuffle, subset, ensmode, rankers)
   return diginetica.loadUnits(diginetica.path_validunits, maxLoad, subset, ensmode, rankers)
end

--- Paired sets loader
function diginetica.loadDataset(fileName, maxLoad, subset, ensmode, rankers)
   if subset ~= 'full' then
      fileName = fileName .. '-' .. subset
   end
   fileName = fileName .. '.t7'

   local f = torch.load(fileName)
   local data = f:type(torch.getdefaulttensortype())

   local nExample = f:size(1)
   if maxLoad and maxLoad > 0 and maxLoad < nExample then
      nExample = maxLoad
      print('<diginetica> loading only ' .. nExample .. ' examples')
   end

   -- NOTE: Lua indexes data from 1
   -- Data entry format: P(A > B), qID, featureA1, ... , feature AN, feature B1, ... , featureBN
   collectgarbage()
   data = data[{{1,nExample}}]
   collectgarbage()

   -- Extract P(A > B) in 'targets' tensor and remove qID from the feature vector
   local targets = data:select(2, 1)
   collectgarbage()
   data = data[{{}, {3, data:size(2)}}]
   collectgarbage()

   local dataset = {}
   dataset.data = torch.Tensor(2, data:size(1), data:size(2) / 2)
   for i=1, data:size(1) do
      dataset.data[1][i] = data[{ i, {1, data:size(2) / 2}}]
      dataset.data[2][i] = data[{ i, {data:size(2) / 2 + 1, data:size(2)}}]

      if i % 200 == 0 then
         collectgarbage()
      end
   end
   dataset.targets = targets

   collectgarbage()
   print('<diginetica> done loading ' .. fileName)

   function dataset:size()
      return nExample
   end

   if ensmode ~= nil and rankers ~= nil then
       print('<diginetica> parsing data for ' .. #rankers .. ' rankers in ' .. ensmode .. ' mode.')

      --- Data required by the ensemble head
      --- Process the dataset through the rankers
      if ensmode == 'simpleNet' then
         local newData = torch.Tensor(2, data:size(1), #rankers)
         -- Get the pair-scores for each ranker in the ensemble.
         -- Glue these scores together to obtain a feature set on which
         -- the ensemble head is trained on.
         for rId, ranker in pairs(rankers) do
            print('<diginetica> parsing data through ranker ' .. rId)

            local pairRanker = nn.ParallelTable()
            pairRanker:add(ranker)
            pairRanker:add(ranker)

            local rankerScores = pairRanker:forward(dataset.data)
            print('<diginetica> parsed data through ranker ' .. rId)

            pairRanker = nil
            collectgarbage()

            for i=1, data:size(1) do
               newData[1][i][rId] = rankerScores[1][i]
               newData[2][i][rId] = rankerScores[2][i]

               if i % 200 == 0 then collectgarbage() end
            end

            rankerScores = nil
            collectgarbage()
         end

         dataset.data = newData
         collectgarbage()

         print('<diginetica> data parsing completed')
      end
      --TODO: Adapt the loading to the other ensemble types.
   end

   setmetatable(dataset, {__index = function(self, index)
			     local input = self.data[{{}, index}]
			     local target = self.targets[index]
			     local example = {input, target}
                 return example
               end})

   return dataset
end

--- Unit sets loader
function diginetica.loadUnits(fileName, maxLoad, subset, ensmode, rankers)
   if subset ~= 'full' then
      fileName = fileName .. '-' .. subset
   end
   fileName = fileName .. '.t7'
end

function diginetica.loadRankers(rankerNames)
   local function split(inputstr, sep)
      local t = {} ; local i = 1

      if sep == nil then sep = "%s" end

      for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
         t[i] = str
         i = i + 1
      end

      return t
   end

   local ranker_names = split(rankerNames, ',')
   local rankers = {}
   for k, ranker_name in pairs(ranker_names) do
      rankers[#rankers + 1] = torch.load('~$trainedLinearRankerdiginetica' .. '-' .. ranker_name .. '.t7')
      rankers[#rankers]:evaluate()
   end

    print('<diginetica> loaded ' .. #rankers .. ' rankers')
   return rankers
end

