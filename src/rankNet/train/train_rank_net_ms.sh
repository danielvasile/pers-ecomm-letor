#!/bin/bash

# set the number of nodes and processes per node
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16

# request memory
#SBATCH --mem 128000

# set max wallclock time
#SBATCH --time=20:00:00

# set name of job
#SBATCH --job-name=rankNetTrainingMS

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=ALL

# send mail to this address
#SBATCH --mail-user=daniel.vasile@oriel.ox.ac.uk

# use submission environment

cd /data/coml-pssr/orie3215/workspace/rankNet/train

# load torch
module load torch

]# start training
th /data/coml-pssr/orie3215/workspace/rankNet/train/rank_net.lua -dataset 'microsoft' -train_size 0 -valid_size 500000 -dropout true -threads 16 > trainResultsMS.txt
