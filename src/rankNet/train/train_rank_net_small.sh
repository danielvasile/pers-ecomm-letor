#!/bin/bash

# set the number of nodes and processes per node
#PBS -l nodes=1:ppn=16

# set max wallclock time
#PBS -l walltime=100:00:00

# set name of job
#PBS -N rankNetTrainingSmall

# mail alert at start, end and abortion of execution
#PBS -m bea

# send mail to this address
#PBS -M daniel.vasile@oriel.ox.ac.uk

# use submission environment
#PBS -V

cd /data/coml-pssr/orie3215/workspace/learning-to-rank/train

# load torch
module load torch

# start training
th /data/coml-pssr/orie3215/workspace/learning-to-rank/train/rank_net_small.lua > trainResultsSmall.txt
