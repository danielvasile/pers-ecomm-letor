#!/bin/bash

# set the number of nodes and processes per node
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16

# request memory
#SBATCH --mem 128000

# set max wallclock time
#SBATCH --time=20:00:00

# set name of job
#SBATCH --job-name=rankNetTrainingDigi

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=ALL

# send mail to this address
#SBATCH --mail-user=daniel.vasile@oriel.ox.ac.uk


cd /data/coml-pssr/orie3215/workspace/rankNet/train

# load torch
module load torch

# Train 3 specialised rankers: full, auth, unauth
th /data/coml-pssr/orie3215/workspace/rankNet/train/rank_net.lua -dataset 'diginetica' -epochs 15 -shuffle false -dropout true > trainResultsDiginetica-full.txt
th /data/coml-pssr/orie3215/workspace/rankNet/train/rank_net.lua -dataset 'diginetica' -epochs 15 -shuffle false -dropout true -subset auth > trainResultsDiginetica-auth.txt
th /data/coml-pssr/orie3215/workspace/rankNet/train/rank_net.lua -dataset 'diginetica' -epochs 15 -shuffle false -dropout true -subset unauth > trainResultsDiginetica-unauth.txt
