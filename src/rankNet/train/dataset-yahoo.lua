require 'torch'
require 'paths'

yahoo = {}

yahoo.path_dataset = '../ltrc_yahoo'
yahoo.path_trainset = paths.concat(yahoo.path_dataset, '~$set1.big.train.t7')
yahoo.path_testset = paths.concat(yahoo.path_dataset, '~$set1.big.test.t7')
yahoo.path_validset = paths.concat(yahoo.path_dataset, '~$set1.valid.t7')

function yahoo.loadTrainSet(maxLoad, shuffle)
   return yahoo.loadDataset(yahoo.path_trainset, maxLoad, shuffle)
end

function yahoo.loadTestSet(maxLoad, shuffle)
   return yahoo.loadDataset(yahoo.path_testset, maxLoad, shuffle)
end

function yahoo.loadValidSet(maxLoad, shuffle)
   return yahoo.loadDataset(yahoo.path_validset, maxLoad, shuffle)
end

function yahoo.loadDataset(fileName, maxLoad, shuffle)
   local f = torch.load(fileName)
   local data = f:type(torch.getdefaulttensortype())

   if shuffle == 'true' then
        -- shuffle the dataset
        local shuffled_indices = torch.randperm(data:size(1)):long()
        -- create a shuffled *copy*, with a new storage
        data = data:index(1, shuffled_indices):squeeze()
   end

   local nExample = f:size(1)
   if maxLoad and maxLoad > 0 and maxLoad < nExample then
      nExample = maxLoad
      print('<yahoo> loading only ' .. nExample .. ' examples')
   end

   -- NOTE: Lua indexes data from 1 (crazy!?)
   -- Data entry format: P(A > B), qID, featureA1, ... , feature A700, feature B1, ... , featureB700
   collectgarbage()
   data = data[{{1,nExample}}]
   collectgarbage()

   -- Extract P(A > B) in 'targets' tensor and remove qID from the feature vector
   local targets = data:select(2, 1)
   collectgarbage()
   data = data[{{}, {3, 1402}}]
   collectgarbage()

   local dataset = {}
   dataset.data = torch.Tensor(2, data:size(1), 700)
   for i=1, data:size(1) do
      dataset.data[1][i] = data[{ i, {1, 700}}]
      dataset.data[2][i] = data[{ i, {701, 1400}}]

      -- print(i)
      if i % 200 == 0 then
         collectgarbage()
      end
   end
   dataset.targets = targets

   collectgarbage()
   print('<yahoo> done')

   function dataset:normalize(mean_, std_)
      local mean = mean or data:view(data:size(1), -1):mean(1)
      local std = std_ or data:view(data:size(1), -1):std(1, true)
      for i=1,data:size(1) do
         data[i]:add(-mean[1][i])
         if std[1][i] > 0 then
            tensor:select(2, i):mul(1/std[1][i])
         end
      end
      return mean, std
   end

   function dataset:normalizeGlobal(mean_, std_)
      local std = std_ or data:std()
      local mean = mean_ or data:mean()
      data:add(-mean)
      data:mul(1/std)
      return mean, std
   end

   function dataset:size()
      return nExample
   end

   setmetatable(dataset, {__index = function(self, index)
			     local input = self.data[{{}, index}]
			     local target = self.targets[index]
			     local example = {input, target}
                                       return example
   end})

   return dataset
end

--- Load test
-- local trainSet = yahoo.loadTrainSet(0)
-- print(trainSet[1])