#!/usr/bin/python

import os,sys,math
from random import shuffle

# Compute and print NDCG score for a query
def NDCG(queryData):
    def computeDCG(targetOrPred):
        # Sort pairs by targetOrPred relevance
        queryData.sort(key=lambda pair: float(pair[targetOrPred]), reverse=True)

        dcg = 0.0
        for j in range(0, len(queryData)):
            rateI = float(queryData[j][2])
            dcg += (pow(2, rateI) - 1) / math.log(2 + j, 2)

        return dcg

    # Compute Ni to normalise target DCG
    targetDCGP = computeDCG(2)
    if targetDCGP > 0:
        ni = 1.0 / targetDCGP
        # Compute predicted NDCG
        predDCGP = computeDCG(3)
        return ni * predDCGP
    else:
        ni = 1.0
        return 1.0

    # Compute predicted NDCG
    # predDCGP = computeDCG(3)
    # return ni * predDCGP

def parse(ensmode, fullRankerResults, authRankerResults, unauthRankerResults):
    lastQid = ''
    queryData = []
    totalNDCGP = 0.0
    queries = 0.0

    if ensmode == "full":
        for l in open(fullRankerResults, 'r'):
            fullLineData = l.rstrip().split(',')
            qid = fullLineData[0]

            if qid == lastQid or lastQid == '':
                queryData.append(fullLineData)
                lastQid = qid
            else:
                predNDCG = NDCG(queryData)
                print ("" + str(lastQid) + "," + str(predNDCG))
                totalNDCGP += predNDCG
                queries += 1

                lastQid = qid
                queryData = [fullLineData]

    else:
        for (fullLine, authLine, unauthLine) in list(zip(open(fullRankerResults, 'r'), open(authRankerResults, 'r'), open(unauthRankerResults, 'r'))):
            fullLineData = fullLine.rstrip().split(',')
            authLineData = authLine.rstrip().split(',')
            unauthLineData = unauthLine.rstrip().split(',')

            qid = fullLineData[0]

            # Compute the ensemble score
            if ensmode == "sum":
                fullLineData[3] = float(fullLineData[3]) + float(authLineData[3]) + float(unauthLineData[3])
            elif ensmode == "hybrid":
                if int(fullLineData[5]) == 1:
                    fullLineData[3] = float(authLineData[3])
                else:
                    fullLineData[3] = float(unauthLineData[3])

            if qid == lastQid or lastQid == '':
                queryData.append(fullLineData)
                lastQid = qid
            else:
                predNDCG = NDCG(queryData)
                print ("" + str(lastQid) + "," + str(predNDCG))
                totalNDCGP += predNDCG
                queries += 1

                lastQid = qid
                queryData = [fullLineData]

    print ("TotalNDCG: " + str(totalNDCGP))
    print ("Average NDCG: " + str(totalNDCGP / queries))
if len(sys.argv)<2:
    print ("Usage: python computeRankingAcc.py ensmode testResults-full testResults-auth testResults-unauth")
else:
    if sys.argv[1] == 'full':
        parse(sys.argv[1], sys.argv[2], "", "")
    else:
        parse(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
