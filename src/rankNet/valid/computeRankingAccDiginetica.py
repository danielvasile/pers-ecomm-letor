#!/usr/bin/python

import os,sys,math
from random import shuffle

# Compute and print NDCG score for a query
def NDCG(docQueryPairs):
    def computeDCG(targetOrPred):
        # Sort pairs by targetOrPred relevance
        docQueryPairs.sort(key=lambda pair: float(pair[targetOrPred]), reverse=True)

        dcg = 0.0
        for j in range(0, len(docQueryPairs)):
            rateI = float(docQueryPairs[j][2])
            dcg += (pow(2, rateI) - 1) / math.log(2 + j, 2)

        return dcg

    # Compute Ni to normalise target DCG
    targetDCGP = computeDCG(2)
    if targetDCGP > 0:
        ni = 1.0 / targetDCGP
        # Compute predicted NDCG
        predDCGP = computeDCG(3)
        return ni * predDCGP
    else:
        ni = 1.0
        return 1.0

    # Compute predicted NDCG
    # predDCGP = computeDCG(3)
    # return ni * predDCGP

def parse(filename):
    lastQid = ''
    docQueryPairs = []
    totalNDCGP = 0.0
    queries = 0.0

    for l in open(filename, 'r'):
        docQueryData = l.rstrip().split(',')
        qid = docQueryData[0]

        if qid == lastQid or lastQid == '':
            docQueryPairs.append(docQueryData)
            lastQid = qid
        else:
            predNDCG = NDCG(docQueryPairs)
            print ("" + str(lastQid) + "," + str(predNDCG))
            totalNDCGP += predNDCG
            queries += 1
            
            lastQid = qid
            docQueryPairs = [docQueryData]

    print ("TotalNDCG: " + str(totalNDCGP))
    print ("Average NDCG: " + str(totalNDCGP / queries))

if len(sys.argv)<2:
    print ("Usage: python computeRankingAcc.py testResults.txt")
else:
    parse(sys.argv[1])
