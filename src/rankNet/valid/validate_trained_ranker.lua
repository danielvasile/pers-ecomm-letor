require 'torch'
require 'paths'
require 'math'
require 'nn'
require 'optim'

cmd = torch.CmdLine()
cmd:option('-rankerPath', '', 'path to the trained ranker')
cmd:option('-validSetPath',  '', 'path to the validation set')
cmd:option('-isDiginetica', '', 'is this validating a ranker for diginetica')
params = cmd:parse(arg)

dataset = {}

dataset.path_linearRanker = params.rankerPath
dataset.path_validset = params.validSetPath

local linearRanker = torch.load(dataset.path_linearRanker)
linearRanker:evaluate()

local f = torch.load(dataset.path_validset)
local data = f:type(torch.getdefaulttensortype())


local targetRelLevels, qIDs, itemIDs, validData
if params.isDiginetica == 'true' then
	targetRelLevels = data:select(2, 4)
	qIDs = data:select(2, 5)
	itemIDs = data:select(2, 2)
	validData = data[{{}, {6, data:size(2)}}]
else
	targetRelLevels = data:select(2, 1)
	qIDs = data:select(2, 2)
	validData = data[{{}, {3, data:size(2)}}]
end

local eval = function(data, batch_size, model)
    local n_data = data:size(1)
    local iters =  math.ceil(n_data / batch_size)

    local predRelLevels = model:forward(data[{{1, math.min(n_data, batch_size)}, {}}])

    for i=2, iters do
        local start_index = (i-1) * batch_size + 1
        local end_index = math.min(n_data, i * batch_size)

        local batch_inputs = data[{{start_index, end_index}, {}}]
        predRelLevels = torch.cat(predRelLevels, model:forward(batch_inputs), 1)
    end

    collectgarbage()

    return predRelLevels
end

local predRelLevels = eval(validData, 10000, linearRanker)

local prevQID = qIDs[1]
local docId = 0

for i=1, data:size(1) do
	local qID = qIDs[i]
	local origRank = 0

	if params.isDiginetica == 'true' then
		origRank = validData[i][3]
		docId = itemIDs[i]
	else
		if qID == prevQID  then
			docId = docId + 1
		else
			prevQID = qID
			docId = 1
		end
	end

	print(string.format("%.0f,%.0f,%.0f,%.6f,%.6f,%.0f", qID, docId,
		targetRelLevels[i], predRelLevels[i][1], origRank, validData[i][1]))
end
