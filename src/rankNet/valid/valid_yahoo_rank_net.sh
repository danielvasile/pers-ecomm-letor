#!/bin/bash

# set the number of nodes and processes per node
#PBS -l nodes=1:ppn=16

# set max wallclock time
#PBS -l walltime=100:00:00

# set name of job
#PBS -N rankNetValidYahoo

# mail alert at start, end and abortion of execution
#PBS -m bea

# send mail to this address
#PBS -M daniel.vasile@oriel.ox.ac.uk

# use submission environment
#PBS -V

cd /data/coml-pssr/orie3215/workspace/rankNet/valid

# load torch
module load torch

# load python
module load python

# estimate relevance levels for the (query, doc) validation pairs
th /data/coml-pssr/orie3215/workspace/rankNet/valid/validate_trained_ranker.lua > '~$yahooValidResults.txt'

# compute the NDGC accuracy
python computeRankingAcc.py '~$yahooValidResults.txt' > '~$yahooValidAccuracy.txt'
