#!/bin/bash

# set the number of nodes and processes per node
#PBS -l nodes=1:ppn=16

# set max wallclock time
#PBS -l walltime=100:00:00

# set name of job
#PBS -N rankNetValidDiginetica

# mail alert at start, end and abortion of execution
#PBS -m bea

# send mail to this address
#PBS -M daniel.vasile@oriel.ox.ac.uk

# use submission environment
#PBS -V

cd /data/coml-pssr/orie3215/workspace/rankNet/valid

# load torch
module load torch

# load python
module load python

# estimate relevance levels for the (query, doc) validation pairs (full set) for full, auth, and unauth rankers
th /data/coml-pssr/orie3215/workspace/rankNet/valid/validate_trained_ranker.lua -rankerPath '../train/~$trainedLinearRankerdiginetica-full.t7' -validSetPath '../../diginetica-challenge/dataset/~$valid-qi-features.t7' -isDiginetica true > '~$digineticaValidResults-full.txt'

th /data/coml-pssr/orie3215/workspace/rankNet/valid/validate_trained_ranker.lua -rankerPath '../train/~$trainedLinearRankerdiginetica-auth.t7' -validSetPath '../../diginetica-challenge/dataset/~$valid-qi-features.t7' -isDiginetica true > '~$digineticaValidResults-auth-onFull.txt'

th /data/coml-pssr/orie3215/workspace/rankNet/valid/validate_trained_ranker.lua -rankerPath '../train/~$trainedLinearRankerdiginetica-unauth.t7' -validSetPath '../../diginetica-challenge/dataset/~$valid-qi-features.t7' -isDiginetica true > '~$digineticaValidResults-unauth-onFull.txt'

# compute the NDGC accuracy for them
python computeRankingAccDiginetica.py '~$digineticaValidResults-full.txt' > '~$digineticaValidAccuracy-full.txt'
python computeRankingAccDiginetica.py '~$digineticaValidResults-auth-onFull.txt' > '~$digineticaValidAccuracy-auth-onFull.txt'
python computeRankingAccDiginetica.py '~$digineticaValidResults-unauth-onFull.txt' > '~$digineticaValidAccuracy-unauth-onFull.txt'

#### Ensemble(s) validation
python computeRankingAccDigineticaSumEns.py 'sum' ~\$digineticaValidResults-full.txt ~\$digineticaValidResults-auth-onFull.txt ~\$digineticaValidResults-unauth-onFull.txt > ~\$digineticaValidAccuracy-sum.txt
python computeRankingAccDigineticaSumEns.py 'hybrid' ~\$digineticaValidResults-full.txt ~\$digineticaValidResults-auth-onFull.txt ~\$digineticaValidResults-unauth-onFull.txt > ~\$digineticaValidAccuracy-hybrid.txt
