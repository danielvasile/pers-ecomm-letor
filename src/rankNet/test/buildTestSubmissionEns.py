#!/usr/bin/python

import os, sys, math


# Sort the query-item pairs and print the submission line for this query
def sortItems(qId, queryItemPairs):
    # Sort pairs by targetOrPred relevance
    queryItemPairs.sort(key=lambda pair: float(pair[3]), reverse=True)

    submissionString = "" + qId + " "
    for qiPair in queryItemPairs:
        submissionString += qiPair[1] + ","

    print (submissionString)


def parse(ensmode, fullRankerResults, authRankerResults, unauthRankerResults):
    lastQid = ''
    queryData = []

    if ensmode == "full":
        for l in open(fullRankerResults, 'r'):
            fullLineData = l.rstrip().split(',')
            qid = fullLineData[0]

            if qid == lastQid or lastQid == '':
                queryData.append(fullLineData)
                lastQid = qid
            else:
                sortItems(lastQid, queryData)

                lastQid = qid
                queryData = [fullLineData]
    else:
        for (fullLine, authLine, unauthLine) in list(zip(open(fullRankerResults, 'r'), open(authRankerResults, 'r'), open(unauthRankerResults, 'r'))):
            fullLineData = fullLine.rstrip().split(',')
            authLineData = authLine.rstrip().split(',')
            unauthLineData = unauthLine.rstrip().split(',')

            qid = fullLineData[0]

            # Compute the ensemble score
            if ensmode == "sum":
                fullLineData[3] = float(fullLineData[3]) + float(authLineData[3]) + float(unauthLineData[3])
            elif ensmode == "hybrid":
                if int(fullLineData[5]) == 1:
                    fullLineData[3] = float(authLineData[3])
                else:
                    fullLineData[3] = float(unauthLineData[3])

            if qid == lastQid or lastQid == '':
                queryData.append(fullLineData)
                lastQid = qid
            else:
                sortItems(lastQid, queryData)

                lastQid = qid
                queryData = [fullLineData]

if len(sys.argv) < 2:
    print ("Usage: python buildTestSubmissionEns.py ensmode testResults-full testResults-auth testResults-unauth")
else:
    if sys.argv[1] == 'full':
        parse(sys.argv[1], sys.argv[2], "", "")
    else:
        parse(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
