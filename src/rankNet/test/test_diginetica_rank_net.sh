#!/bin/bash

# set the number of nodes and processes per node
#PBS -l nodes=1:ppn=16

# set max wallclock time
#PBS -l walltime=100:00:00

# set name of job
#PBS -N rankNetTestDiginetica

# mail alert at start, end and abortion of execution
#PBS -m bea

# send mail to this address
#PBS -M daniel.vasile@oriel.ox.ac.uk

# use submission environment
#PBS -V

cd /data/coml-pssr/orie3215/workspace/rankNet/test

# load torch
module load torch

# load python
module load python

# estimate relevance levels for the (query, doc) test pairs (full set) for full, auth, and unauth rankers
th /data/coml-pssr/orie3215/workspace/rankNet/test/test_trained_ranker.lua -rankerPath '../train/~$trainedLinearRankerdiginetica-full.t7' -testSetPath '../../diginetica-challenge/dataset/~$test-qi-features.t7' -isDiginetica true > '~$digineticaTestResults-full.txt'
th /data/coml-pssr/orie3215/workspace/rankNet/test/test_trained_ranker.lua -rankerPath '../train/~$trainedLinearRankerdiginetica-auth.t7' -testSetPath '../../diginetica-challenge/dataset/~$test-qi-features.t7' -isDiginetica true > '~$digineticaTestResults-auth-onFull.txt'
th /data/coml-pssr/orie3215/workspace/rankNet/test/test_trained_ranker.lua -rankerPath '../train/~$trainedLinearRankerdiginetica-unauth.t7' -testSetPath '../../diginetica-challenge/dataset/~$test-qi-features.t7' -isDiginetica true > '~$digineticaTestResults-unauth-onFull.txt'

# compute the submission files
python buildTestSubmission.py '~$digineticaTestResults-full.txt' | awk '{gsub(/,$/,""); print}' | sort -k1n > '~$digineticaTestSubmission-full.txt'
python buildTestSubmission.py '~$digineticaTestResults-auth-onFull.txt' | awk '{gsub(/,$/,""); print}' | sort -k1n > '~$digineticaTestSubmission-auth-onFull.txt'
python buildTestSubmission.py '~$digineticaTestResults-unauth-onFull.txt' | awk '{gsub(/,$/,""); print}' | sort -k1n > '~$digineticaTestSubmission-unauth-onFull.txt'

#### Ensemble(s) testing
python buildTestSubmissionEns.py 'sum' '~$digineticaTestResults-full.txt' '~$digineticaTestResults-auth-onFull.txt' '~$digineticaTestResults-unauth-onFull.txt' | awk '{gsub(/,$/,""); print}' | sort -k1n > '~$digineticaTestSubmission-sum.txt'
python buildTestSubmissionEns.py 'hybrid' '~$digineticaTestResults-full.txt' '~$digineticaTestResults-auth-onFull.txt' '~$digineticaTestResults-unauth-onFull.txt' | awk '{gsub(/,$/,""); print}' | sort -k1n > '~$digineticaTestSubmission-hybrid.txt'
