#!/bin/bash

# set the number of nodes and processes per node
#PBS -l nodes=1:ppn=16

# set max wallclock time
#PBS -l walltime=100:00:00

# set name of job
#PBS -N rankNetTestMS

# mail alert at start, end and abortion of execution
#PBS -m bea

# send mail to this address
#PBS -M daniel.vasile@oriel.ox.ac.uk

# use submission environment
#PBS -V

cd /data/coml-pssr/orie3215/workspace/rankNet/test

# load torch
module load torch

# load python
module load python

# estimate relevance levels for the (query, doc) validation pairs
th /data/coml-pssr/orie3215/workspace/rankNet/test/test_trained_ranker.lua -rankerPath '../train/~$trainedLinearRankermicrosoft.t7' -testSetPath '../microsoft_data/~$testRel.t7' > microsoftTestResults.txt

# compute the NDGC accuracy
python ../valid/computeRankingAcc.py microsoftTestResults.txt > microsoftTestAccuracy.txt
