#!/usr/bin/python

import os, sys, math


# Sort the query-item pairs and print the submission line for this query
def sortItems(qId, queryItemPairs):
    # Sort pairs by targetOrPred relevance
    queryItemPairs.sort(key=lambda pair: float(pair[3]), reverse=True)

    submissionString = "" + qId + " "
    for qiPair in queryItemPairs:
        submissionString += qiPair[1] + ","

    print (submissionString)


def parse(filename):
    lastQid = ''
    queryItemPairs = []

    for l in open(filename, 'r'):
        queryItemData = l.rstrip().split(',')
        qid = queryItemData[0]

        if qid == lastQid or lastQid == '':
            queryItemPairs.append(queryItemData)
            lastQid = qid
        else:
            sortItems(lastQid, queryItemPairs)

            lastQid = qid
            queryItemPairs = [queryItemData]

if len(sys.argv) < 2:
    print ("Usage: python buildTestSubmission.py testResults.txt")
else:
    parse(sys.argv[1])
