require 'torch'
require 'paths'
require 'math'
require 'nn'
require 'optim'

cmd = torch.CmdLine()
cmd:option('-rankerPath','../train/~$trainedLinearRankeryahoo.t7','path to the trained ranker')
cmd:option('-testSetPath', '../ltrc_yahoo/~$set1.testRel.t7', 'path to the test set')
cmd:option('-isDiginetica', 'false', '')
params = cmd:parse(arg)

dataset = {}

dataset.path_linearRanker = params.rankerPath
dataset.path_testset = params.testSetPath

local linearRanker = torch.load(dataset.path_linearRanker)
linearRanker:evaluate()

-- print ('loaded linearRanker')

local f = torch.load(dataset.path_testset)
local data = f:type(torch.getdefaulttensortype())


local targetRelLevels, qIDs, itemIDs, testData
if params.isDiginetica == 'true' then
	targetRelLevels = data:select(2, 4)
	qIDs = data:select(2, 5)
	itemIDs = data:select(2, 2)
	testData = data[{{}, {6, data:size(2)}}]
else
	targetRelLevels = data:select(2, 1)
	qIDs = data:select(2, 2)
	testData = data[{{}, {3, data:size(2)}}]
end

-- print ('loaded test data')

local eval = function(data, batch_size, model)
    local n_data = data:size(1)
    local iters =  math.ceil(n_data / batch_size)

    local predRelLevels = model:forward(data[{{1, math.min(n_data, batch_size)}, {}}])

    for i=2, iters do
        local start_index = (i-1) * batch_size + 1
        local end_index = math.min(n_data, i * batch_size)

        local batch_inputs = data[{{start_index, end_index}, {}}]
        predRelLevels = torch.cat(predRelLevels, model:forward(batch_inputs), 1)
    end

    collectgarbage()

    return predRelLevels
end

local predRelLevels = eval(testData, 10000, linearRanker)

-- print ('Computed predicted relevance levels:')

-- print ('qID, docID, targetRel, predRel')
local prevQID = qIDs[1]
local itemId = 0

for i=1, data:size(1) do
	local qID = qIDs[i]
	local origRank = 0

	if params.isDiginetica == 'true' then
		origRank = testData[i][3]
		itemId = itemIDs[i]
	else
		if qID == prevQID  then
			itemId = itemId + 1
		else
			prevQID = qID
			itemId = 1
		end
	end

	print(string.format("%.0f,%.0f,%.0f,%.6f,%.6f,%.0f", qID, itemId, targetRelLevels[i], predRelLevels[i][1], origRank, testData[i][1]))
end
