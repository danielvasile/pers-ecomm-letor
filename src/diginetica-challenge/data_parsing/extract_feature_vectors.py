#!/usr/bin/python

import sys
import collections
import argparse
import datetime
from memory_diagnostics import total_size

import pandas as pd
import numpy as np
from sklearn.utils import shuffle
from sklearn import preprocessing
from bisect import *
from math import isnan, exp
import random

random.seed(3)

parser = argparse.ArgumentParser()

parser.add_argument("-f", "--jupFile", default='') #Jupyter Notebook Hack to allow Argparse
parser.add_argument("-d", "--datasetPath", help="path to the dataset", default='../../dataset/')

parser.add_argument("-dr", "--dropFullQueries",
            help="only extract features from query-less data points", action="store_true")

parser.add_argument("-trl", "--trainLimit",
            help="maximum number of train records to extract features from", type=int, default=20000)
parser.add_argument("-tl", "--testLimit",
            help="maximum number of test records to extract features from", type=int, default=2000)

parser.add_argument("-mb", "--maxBatches",
            help="number of parallel runs", type=int, default=0)
parser.add_argument("-b", "--batchId",
            help="id of the current data batch", type=int, default=0)

parser.add_argument("-vs", "--validSplit", help="split ration between valid and train", type=int, default=20)

parser.add_argument("-pn", "--postNormalise",
            help="choose to normalise the (qi, qi) pairs feature vectors instead of the qi vectors",
            action="store_true")
parser.add_argument("-cd", "--consecData",
            help="extract queries from a conescutive time range",
            action="store_true")
parser.add_argument("-l", "--logProgress",
            help="log the progress of the extraction",
            action="store_true")
parser.add_argument("-lm", "--logMemory",
            help="log the RAM usage of the datastructures used",
            action="store_true")

parser.add_argument("-mtrp", "--maxTrainPairs", help="maximum number of (qi, qi) train pairs", type=int, default = -1)
parser.add_argument("-mtrql", "--maxTrainQLoad",
            help="maximum number of items per query in train pairs", type=int, default = 25)

parser.add_argument("-mvp", "--maxValidPairs", help="maximum number of (qi, qi) valid pairs", type=int, default = -1)
parser.add_argument("-mvql", "--maxValidQLoad",
            help="maximum number of items per query in valid pairs", type=int, default = 25)
parser.add_argument("-mvd", "--maxValidData", help="maximum number of qi valid pairs", type=int, default=0)

parser.add_argument("-ms", "--maxSiftFactor", help="maximum sift factor", type = int, default = 20)
parser.add_argument("-zs", "--zeroSiftFactor", help="sift factor in 0-20 of the 0-scored train qi pairs",
            type = int, default = 19)
parser.add_argument("-os", "--oneSiftFactor", help="sift factor in 0-20 of the 1-scored train qi pairs",
            type = int, default = 0)

args = parser.parse_args()


def loadQueries(datasetPath, dropFullQs=False, trainLimit=0, testLimit=0, validSplit=0):
    print ('Loading queries with limits', trainLimit, testLimit)

    train_queries = pd.read_csv(datasetPath + '/~$train-queries.csv', sep=';')

    # Choose how to load training queries (uniformly or consecutively)
    if not args.consecData:
        train_queries = shuffle(train_queries, random_state=3)
    else:  # Loading consecutive data.
        # Sort train queries by event-date to only select data
        # from a short range when running locally.
        train_queries.sort_values(by=['eventdate'], inplace=True)
        fixQueryIndices(train_queries)

    if trainLimit > 0:
        train_queries = train_queries.head(trainLimit)
    fixQueryIndices(train_queries)
    print ("Loaded total train queries", len(train_queries))

    # If not loading consecutive data, this is the time to shuffle, before splitting.
    if not args.consecData:
        train_queries = shuffle(train_queries, random_state=3)
        fixQueryIndices(train_queries)

    (train_queries, valid_queries) = splitTrainValid(train_queries, validSplit)
    fixQueryIndices(train_queries)
    fixQueryIndices(valid_queries)

    test_queries = pd.read_csv(datasetPath + '/~$test-queries.csv', sep=';')
    if testLimit > 0:
        # Only extract test data when not extracting consecutive train data.
        if not args.consecData:
            test_queries = shuffle(test_queries, random_state=3)
            test_queries = test_queries.head(testLimit)
        else:
            test_queries = test_queries.head(1)
    print ("Loaded test queries", len(test_queries))

    if dropFullQs:
        train_queries = dropFullQueries(train_queries)
        print ('Dropped full train queries', len(train_queries))

        valid_queries = dropFullQueries(valid_queries)
        print ('Dropped full valid queries', len(valid_queries))

        test_queries = dropFullQueries(test_queries)
        print ('Dropped full test queries', len(test_queries))

    # Sort by event-date
    train_queries.sort_values(by=['eventdate'], inplace=True)
    valid_queries.sort_values(by=['eventdate'], inplace=True)
    test_queries.sort_values(by=['eventdate'], inplace=True)

    fixQueryIndices(train_queries)
    fixQueryIndices(valid_queries)
    fixQueryIndices(test_queries)

    print ('Completed loading queries.')
    return (train_queries, valid_queries, test_queries)


def fixQueryIndices(queries):
    queries.reset_index(inplace=True)
    queries.drop(['index'], axis=1, inplace=True)


def dropFullQueries(queries):
    return queries[queries['categoryId'] > 0]


def splitTrainValid(train_queries, valid_split_factor):
    train_split = int(len(train_queries) / 100 * (100 - valid_split_factor))
    print ('Splitting train queries into train and valid sets.',
           train_split, len(train_queries) - train_split)

    valid_queries = train_queries.tail(len(train_queries) - train_split)
    train_queries = train_queries.head(train_split)

    return (train_queries, valid_queries)


def countItemActions(actions, sub_actions_start, sub_actions_end, prod_dict,
                     itemCountDict, userCountDict, avgUserCount, userPricePrefs):
    for entry_id in range(sub_actions_start, sub_actions_end):
        itemId = int(actions['itemId'][entry_id])
        event_date = actions['eventdate'][entry_id]
        sessionId = actions['sessionId'][entry_id]
        userId = actions['userId'][entry_id]

        itemCountDict[itemId] += 1
        userCountDict[itemId][userId] += 1

        avgUserCount[itemId]['total'] += 1
        if userCountDict[itemId][userId] == 1:
            # TODO: Think about counting the NA user as a single user,
            # or thinking of NA actions as made by distinct unknown users.
            avgUserCount[itemId]['count'] += 1

        # Some items which logged clicks or purchases might not be part of
        # the product dictionary (not sure why).
        if itemId in prod_dict:
            def recordUserPrefs(specUserPricePrefs, prodPrice):
                specUserPricePrefs['total'] += prodPrice
                specUserPricePrefs['count'] += 1

            recordUserPrefs(userPricePrefs[str(userId)], int(prod_dict[itemId]['price']))
            # For 'NA' users, record global stats across all actions.
            recordUserPrefs(userPricePrefs['NA'], prod_dict[itemId]['price'])
        else:
            print ("Missed itemId in prod_dict", itemId)


# Method to go through all recorded train-actions and process them
# into targetActions and inSessionActions
def countTotalActions(actions, targetActions, inSessionActions, action):
    for entry_id in range(0, len(actions)):
        itemId = int(actions['itemId'][entry_id])
        sId = actions['sessionId'][entry_id]
        time = actions['timeframe'][entry_id]

        # Adding the action to the inSession dictionary
        inSessionActions[action][sId][itemId].append(time)

        # Counting a target action
        if action == 'clicks':
            queryId = actions['queryId'][entry_id]
            targetActions[action][queryId][itemId] += 1
        # Counting a purchase
        elif action == 'purchases':
            targetActions[action][sId][itemId] += 1


def buildProductsDict(products, prod_categories):
    # Build product categories dictionary
    category_dict = {}
    for entry_id in range(0, len(prod_categories)):
        itemId = int(prod_categories['itemId'][entry_id])
        categoryId = prod_categories['categoryId'][entry_id]

        category_dict[itemId] = categoryId

    # Build full products dictionary
    dict = {}
    for entry_id in range(0, len(products)):
        itemId = int(products['itemId'][entry_id]);
        price = products['pricelog2'][entry_id];
        name = products['product.name.tokens'][entry_id];

        dict[itemId] = {'categoryId': category_dict[itemId], 'price': price,
                        'name': name}
    return dict


def defdictToRegular(d):
    # Convert default dictionary to regular, immutable(on get) one
    if isinstance(d, dict):
        d = {k: defdictToRegular(v) for k, v in d.items()}
    return d


def loadCatalogData(datasetPath):
    target_actions = {  # for each query, count (and record) the distinct items which were acted on
        # across the whole time axis to compute the target scores
        'clicks': collections.defaultdict(lambda: collections.defaultdict(int)),
        'purchases': collections.defaultdict(lambda: collections.defaultdict(int))
    }

    session_actions = {  # for each session and each item, record in sorted order
        # the timeframes on which each type of action was performed
        'views': collections.defaultdict(lambda: collections.defaultdict(list)),
        'clicks': collections.defaultdict(lambda: collections.defaultdict(list)),
        'purchases': collections.defaultdict(lambda: collections.defaultdict(list))
    }

    print ("Loading catalog data.")

    # Loading item views
    item_views = pd.read_csv(datasetPath + '/~$train-item-views.csv', sep=';')
    item_views.sort_values(by=['eventdate', 'sessionId', 'timeframe'], inplace=True)
    item_views.reset_index(inplace=True, drop=True)
    print('Item views', len(item_views))

    # Loading clicks
    clicks = pd.read_csv(datasetPath + '/~$train-clicks-wdate.csv', sep=';')
    clicks.sort_values(by=['eventdate', 'sessionId', 'timeframe'], inplace=True)
    clicks.reset_index(inplace=True, drop=True)
    print('Clicks', len(clicks))

    # Loading purchases
    purchases = pd.read_csv(datasetPath + '/~$train-purchases.csv', sep=';')
    purchases.sort_values(by=['eventdate', 'sessionId', 'timeframe'], inplace=True)
    purchases.reset_index(inplace=True, drop=True)
    print('Purchases', len(purchases))

    # Loading products
    products = pd.read_csv(datasetPath + '/~$products.csv', sep=';')
    print ('Products', len(products))

    # Loading product categories
    prod_categories = pd.read_csv(datasetPath + '/~$product-categories.csv', sep=';')
    print('Product categories', len(prod_categories))

    # Building products dictionary
    prod_dict = buildProductsDict(products, prod_categories)
    print('Built products dictionary in memory.')

    # Building target and in-session actions counters
    countTotalActions(item_views, target_actions, session_actions, 'views')
    countTotalActions(clicks, target_actions, session_actions, 'clicks')
    countTotalActions(purchases, target_actions, session_actions, 'purchases')
    print('Built target and in-session actions counters.')

    print ('Completed loading the catalog data.')
    return (item_views, clicks, purchases, prod_dict,
            defdictToRegular(target_actions), defdictToRegular(session_actions))


def getUserActionCount(action, itemId, userId, user_actions, avg_user_actions):
    if isnan(userId):
        return avg_user_actions[action][itemId]['total'] / max(1, avg_user_actions[action][itemId]['count'])
    return user_actions[action][itemId][userId]


def getUserPriceData(action, userId, user_price_prefs):
    if userId == 'nan':
        return user_price_prefs[action]['NA']['total'] / max(1, user_price_prefs[action]['NA']['count'])
    return user_price_prefs[action][userId]['total'] / max(1, user_price_prefs[action][userId]['count'])


def getQueryPriceStats(items):
    (minPrice, maxPrice, totalPrices) = (float("inf"), -float("inf"), 0.0)

    for itemPos in range(0, len(items)):
        itemId = int(items[itemPos])
        minPrice = min(minPrice, prod_dict[itemId]['price'])
        maxPrice = max(maxPrice, prod_dict[itemId]['price'])
        totalPrices += prod_dict[itemId]['price']

    return (minPrice, maxPrice, totalPrices / len(items))


def countDwellTime(user_dwell, categoryId, userId, duration):
    # Firstly, record the total time spent dwelling on this category
    user_dwell['NA'][categoryId]['total'] += duration

    # Then, record the time spent by this user.
    # TODO: Again, think about counting the NA user
    # as a single user (as it is now), or as multiple users.
    user_dwell['users'][categoryId][userId] += duration

    # For the NA user, record an average, by counting
    # the distinct users that have seen this category.
    if user_dwell['users'][categoryId][userId] == duration:
        user_dwell['NA'][categoryId]['count'] += 1


def getDwellTimeData(user_dwell, userId, categoryId):
    total_time = user_dwell['NA'][categoryId]['total']
    user_count = user_dwell['NA'][categoryId]['count']
    if isnan(userId):
        user_time = total_time / max(1, user_count)
    else:
        user_time = user_dwell['users'][categoryId][userId]

    return (user_time, total_time, user_count)


def recordItemImpression(impr_dict, itemId, userId):
    impr_dict['NA'][itemId]['total'] += 1
    impr_dict['users'][itemId][userId] += 1

    if impr_dict['users'][itemId][userId] == 1:
        impr_dict['NA'][itemId]['total'] += 1


def getActionRates(impr_dict, itemId, userId, total_clicks, total_purchases,
                   this_user_clicks, this_user_purchases):
    total_impr = impr_dict['NA'][itemId]['total']
    if isnan(userId):
        user_impr = impr_dict['users'][itemId][userId]
    else:
        user_impr = total_impr / max(1, impr_dict['NA'][itemId]['count'])

    user_impr = max(1, user_impr)
    total_impr = max(1, total_impr)

    return (total_clicks / total_impr, total_purchases / total_impr,
            this_user_clicks / user_impr, this_user_purchases / user_impr)


def buildQueryItemFeatures(queries, sub_queries_start, sub_queries_end,
                           prod_category_dict, prod_actions, session_actions, target_actions, user_actions,
                           avg_user_actions, user_price_prefs, user_dwell,
                           qbatch_start, qbatch_end,
                           zero_sift_factor=0, one_sift_factor=0):
    def inBatch(entry_id):
        return qbatch_start <= entry_id and entry_id < qbatch_end

    queryitem_features = []
    # QueryItem Features Format: -- could record it as dictionary with keys
    # QueryCategoryId, ItemId, ItemCategoryId, ItemQueryScore, QueryId,
    # IsUserAuth, NoItemsReturned, UnpersonalisedRank, UnpersonalisedPercRank,
    # TotalItemClicks, TotalItemViews, TotalItemSales,
    # ClicksUserCount, ViewsUserCount, SalesUserCount,
    # UserClicks, UserViews, UserSales,
    # TotalClickThRate, TotalPurchThRate,
    # UserClickThRate, UserPurchThRate,
    # SessionClicks, SessionViews, SessionSales,
    # Price, UserAvgClickPrice, UserAvgPurchPrice,
    # QueryMinPrice, QueryMaxPrice, QueryAvgPrice,
    # UserCatDwellTime, TotalCatDwellTime, CatDwellUserCount,
    # SearchTokensCount, QueryDuration, QueryWeekDay

    for entry_id in range(sub_queries_start, sub_queries_end):
        items = queries['items'][entry_id].split(",")
        qid = queries['queryId'][entry_id]
        q_date = queries['eventdate'][entry_id]
        q_day = datetime.datetime.strptime(q_date, '%Y-%m-%d').weekday()
        q_time = queries['timeframe'][entry_id]
        sId = queries['sessionId'][entry_id]
        userId = queries['userId'][entry_id]
        duration = queries['duration'][entry_id]  # already in seconds

        # Count the search tokens in a query-full and the dwell time in a category page.
        s_tokens_count = 0
        if pd.notnull(queries['searchstring.tokens'][entry_id]):
            s_tokens_count = len(queries['searchstring.tokens'][entry_id].split(','))
        else:  # Query-less search: count the dwell time
            countDwellTime(user_dwell, queries['categoryId'][entry_id], userId, duration)

        # Record whether user is authenticated or not
        isUserAuth = 1
        if isnan(userId):
            isUserAuth = 0

        # Compute query price stats
        (min_qPrice, max_qPrice, avg_qPrice) = getQueryPriceStats(items)

        for itemPos in range(0, len(items)):
            itemId = int(items[itemPos])
            orig_rank = float(len(items) - itemPos)
            prod_price = float(prod_dict[itemId]['price'])

            # Count the impression of this item to be used for click and purch -through rates
            recordItemImpression(prod_actions['impressions'], itemId, userId)

            # Compute query-item score (0 - no action on item, 1 - click on item, 2 - item was purchased)
            score = 0
            if inBatch(entry_id) and target_actions['clicks'].get(qid, {}).get(itemId, 0) > 0:
                score = 1
                if target_actions['purchases'].get(sId, {}).get(itemId, 0) > 0:
                    score = 2

            sift_choice = random.randint(1, args.maxSiftFactor)
            if inBatch(entry_id) and (not ((score == 0 and sift_choice <= zero_sift_factor) or
                                               (score == 1 and sift_choice <= one_sift_factor))):
                # Etract aggregate clicks, views and purchases for this item up to (and including) query time (date)
                total_clicks = prod_actions['clicks'][itemId]
                total_views = prod_actions['views'][itemId]
                total_purchases = prod_actions['purchases'][itemId]

                # Extract distinct user counts for each action
                user_count_clicks = len(user_actions['clicks'][itemId])
                user_count_views = len(user_actions['views'][itemId])
                user_count_purchases = len(user_actions['purchases'][itemId])

                # Extract action counts for this user
                this_user_clicks = getUserActionCount('clicks', itemId, userId, user_actions, avg_user_actions)
                this_user_views = getUserActionCount('views', itemId, userId, user_actions, avg_user_actions)
                this_user_purchases = getUserActionCount('purchases', itemId, userId, user_actions, avg_user_actions)

                # Extract action counts in this session
                this_ses_clicks = bisect_right(
                    session_actions['clicks'].get(sId, {}).get(itemId, []), q_time)
                this_ses_views = bisect_right(
                    session_actions['views'].get(sId, {}).get(itemId, []), q_time)
                this_ses_purchases = bisect_right(
                    session_actions['purchases'].get(sId, {}).get(itemId, []), q_time)

                # Extract average prices for the clicked, viewed and purchased items for this user
                # TODO: Extract the min and max stats too and add those to the feature vector.
                this_user_avg_click_price = getUserPriceData('clicks', str(userId), user_price_prefs)
                this_user_avg_purchase_price = getUserPriceData('purchases', str(userId), user_price_prefs)

                # Extract dwell time stats
                (this_user_cat_dwell, total_cat_dwell, user_count_cat_dwell) = getDwellTimeData(user_dwell,
                                                                                                userId,
                                                                                                prod_dict[itemId][
                                                                                                    'categoryId'])

                # Extract click and purch -through rates (total, and per user)
                # TODO: Decide whether to include view-through rates (?), or combine views with clicks
                (total_click_rate, total_purch_rate,
                 this_user_click_rate, this_user_purch_rate) = getActionRates(prod_actions['impressions'], itemId,
                                                                              userId, total_clicks, total_purchases,
                                                                              this_user_clicks, this_user_purchases)

                # Record query-item feature vector
                queryitem_features.append([queries['categoryId'][entry_id],
                                           itemId, prod_dict[itemId]['categoryId'], score, qid,
                                           isUserAuth, float(len(items)), orig_rank, orig_rank / len(items),
                                           total_clicks, total_views, total_purchases,
                                           user_count_clicks, user_count_views, user_count_purchases,
                                           this_user_clicks, this_user_views, this_user_purchases,
                                           total_click_rate, total_purch_rate,
                                           this_user_click_rate, this_user_purch_rate,
                                           this_ses_clicks, this_ses_views, this_ses_purchases,
                                           prod_price, this_user_avg_click_price, this_user_avg_purchase_price,
                                           min_qPrice, max_qPrice, avg_qPrice,
                                           this_user_cat_dwell, total_cat_dwell, user_count_cat_dwell,
                                           s_tokens_count, duration, q_day])

    return queryitem_features


def extractFeatureVectors(train_queries, valid_queries, test_queries, item_views, clicks, purchases,
                          prod_category_dict, target_actions, session_actions, log_file):
    print ('Building query-item feature vectors for', len(train_queries), 'train queries,',
           len(valid_queries), 'valid queries and', len(test_queries), 'test queries.')

    # Initialise action data
    prod_actions = {  # for each item, count the number of actions
        'views': collections.defaultdict(int),
        'clicks': collections.defaultdict(int),
        'purchases': collections.defaultdict(int),
        # for each product, record the total SERP impressions (per user);
        # count included for the NA user(s), to retrieve averages
        'impressions': {
            'NA': collections.defaultdict(lambda: {'total': 0, 'count': 0}),
            'users': collections.defaultdict(lambda: collections.defaultdict(int))
        }
    }
    user_actions = {  # for each item, count (and record) the distinct users that acted on it
        'views': collections.defaultdict(lambda: collections.defaultdict(int)),
        'clicks': collections.defaultdict(lambda: collections.defaultdict(int)),
        'purchases': collections.defaultdict(lambda: collections.defaultdict(int))
    }
    # for each item, record the average action count across users
    avg_user_actions = {
        # this will be used as the action count for the NA users
        'views': collections.defaultdict(lambda: {'total': 0, 'count': 0}),
        'clicks': collections.defaultdict(lambda: {'total': 0, 'count': 0}),
        'purchases': collections.defaultdict(lambda: {'total': 0, 'count': 0})
    }
    user_price_prefs = {  # for each user, record basic stats for the prices of the
        # viewed, clicked and purchased items;
        # aggregate data for views and clicks together.
        'clicks': collections.defaultdict(lambda: {'total': 0, 'count': 0}),
        'purchases': collections.defaultdict(lambda: {'total': 0, 'count': 0})
    }
    # for each category, record the dwell time of each user;
    # for the NA user(s), record the average dwell time
    user_dwell = {
        'NA': collections.defaultdict(lambda: {'total': 0, 'count': 0}),
        'users': collections.defaultdict(lambda: collections.defaultdict(int))
    }

    # Initialise query data
    (train_qi_features, valid_qi_features, test_qi_features) = ([], [], [])

    # Define the time interval
    curDate = datetime.datetime.strptime(min(min(train_queries['eventdate'][0], test_queries['eventdate'][0]),
                                             valid_queries['eventdate'][0]), '%Y-%m-%d')
    endDate = datetime.datetime.strptime(max(max(train_queries['eventdate'][len(train_queries) - 1],
                                                 test_queries['eventdate'][len(test_queries) - 1]),
                                             valid_queries['eventdate'][len(valid_queries) - 1]),
                                         '%Y-%m-%d')

    # Compute the query-batch intervals:
    if args.maxBatches > 0:
        train_bsize = int(len(train_queries) / args.maxBatches)
        test_bsize = int(len(test_queries) / args.maxBatches)
        valid_bsize = int(len(valid_queries) / args.maxBatches)

        (train_bstart, train_bend) = (train_bsize * args.batchId, train_bsize * (args.batchId + 1))
        (test_bstart, test_bend) = (test_bsize * args.batchId, test_bsize * (args.batchId + 1))
        (valid_bstart, valid_bend) = (valid_bsize * args.batchId, valid_bsize * (args.batchId + 1))

        if args.batchId == args.maxBatches - 1:
            (train_bend, test_bend, valid_bend) = (len(train_queries), len(test_queries), len(valid_queries))

        print("Extracting query data from batch", args.batchId, "out of", args.maxBatches, "batches.")
    else:
        (train_bstart, train_bend) = (0, len(train_queries))
        (test_bstart, test_bend) = (0, len(test_queries))
        (valid_bstart, valid_bend) = (0, len(valid_queries))

    if args.consecData:
        # ReDefine the time interval to exclude test data to get consecutive training data
        curDate = datetime.datetime.strptime(min(train_queries['eventdate'][0],
                                                 valid_queries['eventdate'][0]), '%Y-%m-%d')
        endDate = datetime.datetime.strptime(max(train_queries['eventdate'][len(train_queries) - 1],
                                                 valid_queries['eventdate'][len(valid_queries) - 1]),
                                             '%Y-%m-%d')

        print ("Extracting consecutive train data. Starting and end dates for query data:", curDate, endDate)

    # Initialise start indices for actions and queries
    (sub_item_views_start, sub_clicks_start, sub_purchases_start) = (0, 0, 0)
    (sub_train_qs_start, sub_valid_qs_start, sub_test_qs_start) = (0, 0, 0)

    # Build the query-item features based on past catalog data only
    while curDate <= endDate:
        # Get start indices for actions and queries
        curDateISO = curDate.strftime('%Y-%m-%d')

        # Log extraction progress:
        if args.logProgress:
            print(curDateISO, file=log_file)
            log_file.flush()

        sub_item_views_end = min(bisect_left(item_views['eventdate'], curDateISO) + 1, len(item_views))
        sub_clicks_end = min(bisect_left(clicks['eventdate'], curDateISO) + 1, len(clicks))
        sub_purchases_end = min(bisect_left(purchases['eventdate'], curDateISO) + 1, len(purchases))

        sub_train_qs_end = bisect_right(train_queries['eventdate'], curDateISO)
        sub_valid_qs_end = bisect_right(valid_queries['eventdate'], curDateISO)
        sub_test_qs_end = bisect_right(test_queries['eventdate'], curDateISO)

        # Parse catalog actions with eventDate < curDate
        countItemActions(item_views, sub_item_views_start, sub_item_views_end, prod_category_dict,
                         prod_actions['views'], user_actions['views'], avg_user_actions['views'],
                         user_price_prefs['clicks'])

        countItemActions(clicks, sub_clicks_start, sub_clicks_end, prod_category_dict,
                         prod_actions['clicks'], user_actions['clicks'], avg_user_actions['clicks'],
                         user_price_prefs['clicks'])

        countItemActions(purchases, sub_purchases_start, sub_purchases_end, prod_category_dict,
                         prod_actions['purchases'], user_actions['purchases'], avg_user_actions['purchases'],
                         user_price_prefs['purchases'])

        # Parse query data with eventDate <= curDate
        train_qi_features += buildQueryItemFeatures(train_queries, sub_train_qs_start, sub_train_qs_end,
                                                    prod_category_dict, prod_actions, session_actions, target_actions,
                                                    user_actions,
                                                    avg_user_actions, user_price_prefs, user_dwell,
                                                    train_bstart, train_bend,
                                                    zero_sift_factor=args.zeroSiftFactor,
                                                    one_sift_factor=args.oneSiftFactor)
        valid_qi_features += buildQueryItemFeatures(valid_queries, sub_valid_qs_start, sub_valid_qs_end,
                                                    prod_category_dict, prod_actions, session_actions, target_actions,
                                                    user_actions,
                                                    avg_user_actions, user_price_prefs, user_dwell,
                                                    valid_bstart, valid_bend)
        test_qi_features += buildQueryItemFeatures(test_queries, sub_test_qs_start, sub_test_qs_end,
                                                   prod_category_dict, prod_actions, session_actions, target_actions,
                                                   user_actions,
                                                   avg_user_actions, user_price_prefs, user_dwell,
                                                   test_bstart, test_bend)

        # Update start indices for actions and queries
        (sub_item_views_start, sub_clicks_start, sub_purchases_start) = (sub_item_views_end,
                                                                         sub_clicks_end, sub_purchases_end)
        (sub_train_qs_start, sub_valid_qs_start, sub_test_qs_start) = (sub_train_qs_end,
                                                                       sub_valid_qs_end, sub_test_qs_end)

        curDate += datetime.timedelta(days=1)

    # Log memory usage for the data built by extractFeatures
    if args.logMemory:
        print("Product actions dict size:", total_size(prod_actions))
        print("User actions dict size:", total_size(user_actions))
        print("AVG user actions actions dict size:", total_size(avg_user_actions))
        print("User price prefs dict size:", total_size(user_price_prefs))
        print("User dwell dict size:", total_size(user_dwell))
        print("Train qi-features uncompr size:", total_size(train_qi_features))
        print("Valid qi-features uncompr size:", total_size(valid_qi_features))
        print("Test qi-features uncompr size:", total_size(test_qi_features))

    return (np.array(train_qi_features), np.array(valid_qi_features), np.array(test_qi_features))

def printScoreStats(qi_featuers, qi_type = '', score_position = 3):
    # Print query-item score stats:
    print (qi_type + 'Pairs with score 0:', len([0 for p in qi_featuers if p[score_position] == 0]))
    print (qi_type + 'Pairs with score 1:', len([1 for p in qi_featuers if p[score_position] == 1]))
    print (qi_type + 'Pairs with score 2:', len([2 for p in qi_featuers if p[score_position] == 2]))

def printGeneralScoreStats(train_queryitem_features, valid_queryitem_features,
            test_queryitem_features, score_position = 3):
    printScoreStats(train_queryitem_features, qi_type = 'Train', score_position = 3)
    print ()
    printScoreStats(valid_queryitem_features, qi_type = 'Valid', score_position = 3)
    print ()
    printScoreStats(test_queryitem_features, qi_type = 'Test', score_position = 3)


# TODO: Disable normalisation for batched extraction.
# TODO: Develop normalising script to work on final data.
def normaliseQIFeatures(norm_data_start=6):  # Works on global feature matrices
    if True or args.maxBatches == 0:
        print ('Normalising query-item feature matrices.')

        train_scaler = preprocessing.StandardScaler().fit(train_qi_features[:, norm_data_start:])
        train_qi_features[:, norm_data_start:] = train_scaler.transform(train_qi_features[:, norm_data_start:])
        valid_qi_features[:, norm_data_start:] = train_scaler.transform(valid_qi_features[:, norm_data_start:])

        # Test data is only extracted when not loading consecutive training data
        if not args.consecData:
            test_qi_features[:, norm_data_start:] = train_scaler.transform(test_qi_features[:, norm_data_start:])
    else:
        print ('Extracting in batches; cannot normalise.')


def normaliseQIPairs():  # Works on global pair matrices
    if True or args.maxBatches == 0:
        print ('Normalising (query-item, query-item) pair matrices.')

        train_scaler = preprocessing.StandardScaler().fit(train_qi_pairs)
        train_qi_pairs = train_scaler.transform(train_qi_pairs)
        valid_qi_pairs = train_scaler.transform(valid_qi_pairs)
    else:
        print ('Extracting in batches; cannot normalise.')


def buildQIPairs(qi_features, maxLoad, maxQueryLoad, score_position=0, qid_position=1):
    print ('Building (query-item, query-item) pairs to learn on with max loads',
           maxLoad, maxQueryLoad, 'on', len(qi_features), 'entries.')
    lastQid = ''
    items = []

    pair_qi_features = []

    for qi_vector in qi_features:
        qid = qi_vector[qid_position]

        if qid == lastQid or lastQid == '':
            items.append(qi_vector)
            lastQid = qid
        else:
            # Compute the query load-limit - the maximum number of items
            # each of the ~140 items per query is paired with (out of ~140).
            limit = min(maxQueryLoad, len(items))
            if limit == -1:
                limit = len(items)

            # Sort the items by descending scores - this way it is guaranteed
            # that the non-zero items will always be paired with and to.
            items.sort(key=lambda qi: qi[score_position], reverse=True)

            # Build the pairs within the specified limits.
            for a in range(0, limit):
                A = items[a]
                oA = A[0]

                for b in range(a + 1, len(items)):
                    B = items[b]
                    oB = B[0]

                    oAB = oA - oB
                    e_oAB = exp(oAB)
                    pAB = e_oAB / (1 + e_oAB)

                    # Data entry format: P(A > B), qID, featureA1, ... , feature AN, feature B1, ... , featureBN
                    pair_qi_features.append(np.concatenate(([pAB], A[1:len(A)], B[2:len(B)])))

            lastQid = qid
            items = [qi_vector]

        if maxLoad > 0 and len(pair_qi_features) > maxLoad:
            break;

    # Log memory used to store the formed (qi, qi) pairs
    if args.logMemory:
        print("QI,QI pairs uncompr size:", total_size(pair_qi_features))

    return np.array(pair_qi_features)


commonCsvHeader = "isUserAuth,itemsReturnedCount,unpersonalisedRank,unpersonalisedPercRank," \
                  "totalItemClicks,totalItemViews,totalItemSales," \
                  "userCountClicks,userCountViews,userCountSales," \
                  "userClicks,userViews,userSales," \
                  "totalClickThRate,totalPurchThRate," \
                  "userClickThRate,userPurchThRate," \
                  "sessionClicks,sessionViews,sessionSales," \
                  "price,userAvgClickPrice,userAvgPurchPrice," \
                  "queryMinPrice,queryMaxPrice,queryAvgPrice," \
                  "userCatDwellTime,totalCatDwellTime,userCountCatDwell," \
                  "searchTokensCount,queryDuration,queryDay"

# Compute batch termination for file names
batchTerm = ""
if args.maxBatches > 0:
    batchTerm = str(args.batchId)


def saveFeaturesCSV(datasetPath, train_qi_features, valid_qi_features, test_qi_features):
    print ('Saving query-item feature matrices into CSV.')

    csvHeader = "queryCategoryId,itemId,itemCategoryId,itemQueryScore,queryId," + commonCsvHeader

    columnsFormat = ['%.0f' for i in range(0, 5)] + ['%.3f' for i in range(5, train_qi_features.shape[1])]

    np.savetxt(datasetPath + '~$train-qi-features' + batchTerm + '.csv',
               train_qi_features, delimiter=',', header=csvHeader, fmt=columnsFormat)

    if args.maxValidData > 0:
        valid_qi_features_tosave = valid_qi_features[:args.maxValidData, :]
    else:
        valid_qi_features_tosave = valid_qi_features
    np.savetxt(datasetPath + '~$valid-qi-features' + batchTerm + '.csv',
               valid_qi_features_tosave, delimiter=',', header=csvHeader, fmt=columnsFormat)

    # Test data is only extracted when not loading consecutive training data
    if not args.consecData:
        np.savetxt(datasetPath + '~$test-qi-features' + batchTerm + '.csv',
                   test_qi_features, delimiter=',', header=csvHeader, fmt=columnsFormat)


def saveMLFeaturesCSV(datasetPath, train_qi_features, valid_qi_features, test_qi_features):
    print ('Saving query-item feature matrices for ML into CSV.')

    csvHeader = "itemQueryScore,queryId," + commonCsvHeader
    columnsFormat = ['%.0f', '%.0f'] + ['%.3f' for i in range(2, train_qi_features.shape[1] - 3)]

    np.savetxt(datasetPath + '~$train-qi-ml-features' + batchTerm + '.csv',
               train_qi_features[:, 3:], delimiter=',', header=csvHeader, fmt=columnsFormat)
    np.savetxt(datasetPath + '~$valid-qi-ml-features' + batchTerm + '.csv',
               valid_qi_features[:, 3:], delimiter=',', header=csvHeader, fmt=columnsFormat)

    # Test data is only extracted when not loading consecutive training data
    if not args.consecData:
        np.savetxt(datasetPath + '~$test-qi-ml-features' + batchTerm + '.csv',
                   test_qi_features[:, 3:], delimiter=',', header=csvHeader, fmt=columnsFormat)


def saveQIPairsCSV(datasetPath, train_qi_pairs, valid_qi_pairs):
    print ('Saving (query-item, query-item) pairs into CSV.')

    csvHeader = "pairScore,queryId," + commonCsvHeader + "," + commonCsvHeader
    columnsFormat = ['%.3f', '%.0f'] + ['%.3f' for i in range(2, train_qi_pairs.shape[1])]

    # Shuffle the QI Pairs before saving
    np.random.seed(3)
    np.random.shuffle(train_qi_pairs)
    np.random.shuffle(valid_qi_pairs)

    np.savetxt(datasetPath + '~$train-qi-pairs' + batchTerm + '.csv',
               train_qi_pairs, delimiter=',', header=csvHeader, fmt=columnsFormat)
    np.savetxt(datasetPath + '~$valid-qi-pairs' + batchTerm + '.csv',
               valid_qi_pairs, delimiter=',', header=csvHeader, fmt=columnsFormat)


def siftQIFeatures(qi_features, zero_sift_factor, one_sift_factor, score_position=3, qi_type=''):
    print ('Sifting', len(qi_features), qi_type + '-qi-features items.')
    print ('Score stats for the original ' + qi_type + ' data:')
    printScoreStats(qi_features, score_position=3, qi_type=qi_type)

    sift_qi_features = []
    for qi_feature in qi_features:
        score = qi_feature[score_position]
        sift_choice = random.randint(1, args.maxSiftFactor)
        if not ((score == 0 and sift_choice <= zero_sift_factor) or
                    (score == 1 and sift_choice <= one_sift_factor)):
            sift_qi_features.append(qi_feature)

    print ('Score stats for the sifted ' + qi_type + ' data:')
    printScoreStats(sift_qi_features, score_position=3, qi_type=qi_type)

    return np.array(sift_qi_features)

# Load catalog data
(item_views, clicks, purchases, prod_dict, target_actions, session_actions) = loadCatalogData(args.datasetPath)

# Log memory consumed by catalog data
if args.logMemory:
    print("Item views dict size: ", total_size(item_views))
    print("Clicks dict size: ", total_size(clicks))
    print("Purchases dict size: ", total_size(purchases))
    print("Products dict size: ", total_size(prod_dict))
    print("Target actions dict size: ", total_size(target_actions))
    print("Session actions dict size: ", total_size(session_actions))

# Loading queries
(train_queries, valid_queries, test_queries) = loadQueries(args.datasetPath,
        dropFullQs = args.dropFullQueries, testLimit = args.testLimit, trainLimit = args.trainLimit,
        validSplit = args.validSplit)

# Log memory usage for loaded query data
if args.logMemory:
    print("Train queries size:", total_size(train_queries))
    print("Valid queries size:", total_size(valid_queries))
    print("Test queries size:", total_size(test_queries))

# Building query-item feature vectors for test and train data
with open('extract_log_file' + str(args.batchId) + '.txt', 'w') as log_file:
    (train_qi_features, valid_qi_features, test_qi_features) = extractFeatureVectors(train_queries,
                valid_queries, test_queries, item_views, clicks, purchases, prod_dict,
                target_actions, session_actions, log_file)

print ('Built Train QueryItem Features', len(train_qi_features))
print ('Built Valid QueryItem Features', len(valid_qi_features))
print ('Built Test QueryItem Features', len(test_qi_features))

# Log memory usage for compressed qi features
if args.logMemory:
    print("Train qi-features compr size:", total_size(train_qi_features))
    print("Valid qi-features compr size:", total_size(valid_qi_features))
    print("Test qi-features compr size:", total_size(test_qi_features))

printGeneralScoreStats(train_qi_features, valid_qi_features, test_qi_features)

# Normalising feature matrices
if not args.postNormalise:
    normaliseQIFeatures()

# Saving train, valid and test features as CSV files
saveFeaturesCSV(args.datasetPath, train_qi_features, valid_qi_features, test_qi_features)
saveMLFeaturesCSV(args.datasetPath, train_qi_features, valid_qi_features, test_qi_features)

# Building train and validation (qi, qi) pairs to be used by RankNet
train_qi_pairs = buildQIPairs(train_qi_features[:,3:], args.maxTrainPairs, args.maxTrainQLoad)
print('Train QI pairs', len(train_qi_pairs))
print('Train matrix shape', train_qi_pairs.shape)

# For valid qi pairs, the valid qi features need to be sifted first, using the same factors as train
valid_qi_features = siftQIFeatures(valid_qi_features, args.zeroSiftFactor, args.oneSiftFactor, qi_type = 'valid')
valid_qi_pairs = buildQIPairs(valid_qi_features[:,3:], args.maxValidPairs, args.maxValidQLoad)
print('Valid QI pairs', len(valid_qi_pairs))
print('Valid matrix shape', valid_qi_pairs.shape)

if args.postNormalise:
    normaliseQIPairs()

# Log memory usage for compressed (qi, qi) pairs
if args.logMemory:
    print("Train qi-pairs compr size:", total_size(train_qi_pairs))
    print("Valid qi-pairs compr size:", total_size(valid_qi_pairs))

# Log memory usage for catalog data after feature extraction
if args.logMemory:
    print("Item views dict size: ", total_size(item_views))
    print("Clicks dict size: ", total_size(clicks))
    print("Purchases dict size: ", total_size(purchases))
    print("Products dict size: ", total_size(prod_dict))
    print("Target actions dict size: ", total_size(target_actions))
    print("Session actions dict size: ", total_size(session_actions))

saveQIPairsCSV(args.datasetPath, train_qi_pairs, valid_qi_pairs)

print ('Feature extraction done.')
print ('Results in:', args.datasetPath)
