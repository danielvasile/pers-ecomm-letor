#!/bin/bash

# set the number of nodes and processes per node
#PBS -l nodes=1:ppn=16

# set max wallclock time
#PBS -l walltime=100:00:00

# set name of job
#PBS -N DigineticaDataParsingBatched

# mail alert at start, end and abortion of execution
#PBS -m bea

# send mail to this address
#PBS -M daniel.vasile@oriel.ox.ac.uk

# use submission environment
#PBS -V

cd /data/coml-pssr/orie3215/workspace/diginetica-challenge/naive_learning_to_rank/data_parsing

# load torch
module load torch

# load python
module load python/anaconda3

# Run the feature extraction script
python extract_feature_vectors.py -d '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -trl 200000 -tl 0 -l -lm -mb 5 -b 2

# Convert the CSV files created to torch binaries
#./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$train-qi-pairs'
#./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$valid-qi-pairs'

#./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$train-qi-features'
#./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$valid-qi-features'
#./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$test-qi-features'