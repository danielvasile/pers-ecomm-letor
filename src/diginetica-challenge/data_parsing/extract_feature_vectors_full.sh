#!/bin/bash

# set the number of nodes and processes per node
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16

# request memory
#SBATCH --mem 256000

# set max wallclock time
#SBATCH --time=20:00:00

# set name of job
#SBATCH --job-name=DigineticaDataParsingFull

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=ALL

# send mail to this address
#SBATCH --mail-user=daniel.vasile@oriel.ox.ac.uk

cd /data/coml-pssr/orie3215/workspace/diginetica-challenge/naive_learning_to_rank/data_parsing

# load torch
module load torch

# load python
module load python/anaconda3

# Run the feature extraction script
python extract_feature_vectors.py -d '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -trl 200000 -tl 0 -l -lm

# Extract subsets using awk
awk -F, '$3 == "isUserAuth" || $3 == 1' '../../dataset/~$train-qi-pairs.csv' > '../../dataset/~$train-qi-pairs-auth.csv'
awk -F, '$3 == "isUserAuth" || $3 != 1' '../../dataset/~$train-qi-pairs.csv' > '../../dataset/~$train-qi-pairs-unauth.csv'
awk -F, '$3 == "isUserAuth" || $3 == 1' '../../dataset/~$valid-qi-pairs.csv' > '../../dataset/~$valid-qi-pairs-auth.csv'
awk -F, '$3 == "isUserAuth" || $3 != 1' '../../dataset/~$valid-qi-pairs.csv' > '../../dataset/~$valid-qi-pairs-unauth.csv'

awk -F, '$6 == "isUserAuth" || $6 == 1' '../../dataset/~$valid-qi-features.csv' > '../../dataset/~$valid-qi-features-auth.csv'
awk -F, '$6 == "isUserAuth" || $6 != 1' '../../dataset/~$valid-qi-features.csv' > '../../dataset/~$valid-qi-features-unauth.csv'


# Convert the CSV files created to torch binaries
./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$train-qi-pairs'
./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$train-qi-pairs-auth'
./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$train-qi-pairs-unauth'

./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$valid-qi-pairs'
./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$valid-qi-pairs-auth'
./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$valid-qi-pairs-unauth'

./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$train-qi-features'
./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$test-qi-features'

./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$valid-qi-features'
./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$valid-qi-features-auth'
./csv_to_t7.lua -datasetPath '/data/coml-pssr/orie3215/workspace/diginetica-challenge/dataset/' -fileName '~$valid-qi-features-unauth'